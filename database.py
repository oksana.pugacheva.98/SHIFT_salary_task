from sqlalchemy import create_engine, ForeignKey
from sqlalchemy.orm import declarative_base, sessionmaker, relationship
from sqlalchemy import Column, Integer, String, Date, DateTime, Boolean


SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"
engine = create_engine(SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False})
Base = declarative_base()


class UserDB(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, index=True)
    login = Column(String, unique=True)
    password = Column(String)
    name = Column(String)
    tokens = relationship("UserTokenDB")
    salaries = relationship("UserSalaryDB", uselist=False)


class UserSalaryDB(Base):
    __tablename__ = "user_salary"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    salary = Column(Integer)
    salaryIncreaseDate = Column(Date)
    user = relationship("UserDB", uselist=False)


class UserTokenDB(Base):
    __tablename__ = "user_token"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    token = Column(String)
    timeCreation = Column(DateTime)
    status = Column(Boolean)
    user = relationship("UserDB")

SessionLocal = sessionmaker(autoflush=False, bind=engine)

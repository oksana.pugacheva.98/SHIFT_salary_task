//обработчик кнопки "Узнать зарплату"
document.getElementById("giveMoney").addEventListener("click", async () => {
    const userToken = document.getElementById("placeForToken").value;
    const response = await fetch("/salary/info", {
        method: "POST",
        headers: { "Accept": "application/json", "Content-Type": "application/json" },
            body: JSON.stringify({
            token: userToken
        })
    });
    if (response.ok === true) {
        const answer = await response.json();
        salaryTable(answer);

    }
    else {
        const error = await response.json();;
        var errorText = document.createElement("p");
        errorText.innerHTML = error.message;
        var place = document.querySelector(".btnMoney");
        errorText.setAttribute("class", "btnM");
        place.appendChild(errorText);
        setTimeout(() => errorText.remove(), 3000);
        console.log(error.message);
    }
});

//функция создания таблицы
function salaryTable(user){
    //если не сущ. таблицы - создаем
    if (!document.querySelectorAll("table").length) {
        var myTable = document.createElement("table");
        myTable.setAttribute("id", "myTable");
        document.body.appendChild(myTable);

        var myHead = myTable.createTHead();
        var row = myHead.insertRow();
        var cell1 = row.insertCell();
        var cell2 = row.insertCell();
        var cell3 = row.insertCell();
        cell1.innerHTML = "Логин";
        cell2.innerHTML = "Зарплата";
        cell3.innerHTML = "Дата повышения";
        var tBody = myTable.createTBody();
        tBody.setAttribute("id", "myBody")

}   //если в тб нет записи о юзере с таким токеном - добавляем
    if(!document.getElementById(user.user_id + user.login)){
        var table = document.getElementById("myBody");
        var row = table.insertRow();
        var loginCell = row.insertCell();
        loginCell.setAttribute("id", user.user_id + user.login)
        var salaryCell = row.insertCell();
        var increaseDateCell = row.insertCell();
        loginCell.innerHTML = user.login;
        salaryCell.innerHTML = user.salary;
        increaseDateCell.innerHTML = user.salary_increase_date;
    }
}
//обработчик кнопки "Сгенерировать"(токен)
document.getElementById("generateBtn").addEventListener("click", async () => {
    const userLogin = document.getElementById("userLogin").value;
    const userPassword = document.getElementById("userPassword").value;
        const response = await fetch("/token/create", {
    method: "POST",
    headers: { "Accept": "application/json", "Content-Type": "application/json" },
        body: JSON.stringify({
        login: userLogin,
        password: userPassword
        })
    });
    if (response.ok === true) {
        const newToken = await response.json();
        document.getElementsByClassName('token_text')[0].textContent = newToken.token;
    }
    else {
        const error = await response.json();
        console.log(error.message);
        document.getElementsByClassName('token_text')[0].textContent = "Пользователь не найден";
    }
});
import secrets

from sqlalchemy.orm import Session
from src.schemas import UserIn, TokenAnswer, TokenIn, SalaryInfoAnswer
from database import *
import uvicorn
from fastapi import FastAPI, Depends
from fastapi.staticfiles import StaticFiles
from fastapi.responses import JSONResponse, FileResponse
from datetime import datetime, timedelta

Base.metadata.create_all(bind=engine)
app = FastAPI()
app.mount("/public", StaticFiles(directory="public"))


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/")
async def root():
    return FileResponse("public/homepage.html")


@app.get("/salaryInfo")
async def salary_page():
    return FileResponse("public/salaryInfo.html")


@app.post("/token/create", response_model=TokenAnswer)
def create_token(user_in: UserIn, db: Session = Depends(get_db)):
    user_out = db.query(UserDB).filter(UserDB.login == user_in.login).first()
    if user_out is None or user_out.password != user_in.password:
        return JSONResponse(status_code=400, content={"message": "Неверные данные"})
    else:
        for token in user_out.tokens:
            token.status = False
            db.add(token)
            db.commit()
            db.refresh(token)
        personal_token = secrets.token_hex(24)
        current_datetime = datetime.now()
        user_token = UserTokenDB(user_id=user_out.id, token=personal_token, timeCreation=current_datetime, status=True)
        db.add(user_token)
        db.commit()
        db.refresh(user_token)
        return {"user_id": user_out.id, "login": user_out.login, "token": personal_token,
                "time_creation": current_datetime}


@app.post("/salary/info", response_model=SalaryInfoAnswer)
def salary_info(token_in: TokenIn, db: Session = Depends(get_db)):
    token_db = db.query(UserTokenDB).filter(UserTokenDB.token == token_in.token).first()
    if token_db is None or token_db.status is False:
        return JSONResponse(status_code=404, content={"message": "Токен не найден"})
    token_lifetime = timedelta(minutes=30)
    if datetime.now() - token_db.timeCreation > token_lifetime:
        token_db.status = False
        db.commit()
        db.refresh(token_db)
        return JSONResponse(status_code=404, content={"message": "Токен не действителен"})
    if token_db.user.salaries is None:
        return JSONResponse(status_code=404, content={"message": "Нет информации о зарплате"})
    return {"user_id": token_db.user.id, "login": token_db.user.login, "salary": token_db.user.salaries.salary,
            "salary_increase_date": token_db.user.salaries.salaryIncreaseDate}


def start_uvicorn():
    uvicorn.run("src.main:app", port=5000, reload=True)

from pydantic import BaseModel
from datetime import datetime, date


class UserIn(BaseModel):
    login: str
    password: str


class TokenIn(BaseModel):
    token: str


class UserAnswer(BaseModel):
    exists: bool
    message: str


class TokenAnswer(BaseModel):
    user_id: int
    login: str
    token: str
    time_creation: datetime


class SalaryInfoAnswer(BaseModel):
    user_id: int
    login: str
    salary: int
    salary_increase_date: date
